package com.narbase.narcore.persistence

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 3/29/17.
 */

class PersistenceManager(context: Context) {

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    @SuppressLint("CommitPrefEdits")
    private var editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun deleteAll() {
        val baseUrl = this.baseUrl
        val cachedInUsername = this.cachedLoggedInUsername

        editor.clear().apply()

        this.baseUrl = baseUrl
        this.cachedLoggedInUsername = cachedInUsername
    }

    var isLoggedIn: Boolean
        get() = sharedPreferences.getBoolean(LOGIN_STATE, false)
        set(state) = editor.putBoolean(LOGIN_STATE, state).apply()

    var accessToken: String
        get() = sharedPreferences.getString(ACCESS_TOKEN, PersistenceManager.Companion.NO_TOKEN)
        set(accessToken) = editor.putString(ACCESS_TOKEN, accessToken).apply()

    var baseUrl: String
        get() = sharedPreferences.getString(BASE_URL_NAME, BASE_URL)
        set(value) = editor.putString(BASE_URL_NAME, value).apply()

    var cachedLoggedInUsername: String
        get() = sharedPreferences.getString(CACHED_LOGGED_IN_USERNAME, "")
        set(value) = editor.putString(CACHED_LOGGED_IN_USERNAME, value).apply()

    companion object {
        private const val PREFS_NAME = "PassengerSharedPreferences"
        private const val LOGIN_STATE = "LOGIN_STATE"
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"
        private const val CACHED_LOGGED_IN_USERNAME = "CACHED_LOGGED_IN_USERNAME"
        private const val NO_TOKEN = ""
        private const val BASE_URL_NAME = "BASE_URL_NAME"
        private const val BASE_URL = "http://165.227.139.152:4567/"
    }

}
