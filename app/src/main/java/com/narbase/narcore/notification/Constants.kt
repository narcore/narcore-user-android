package com.narbase.narcore.notification

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 5/25/17.
 */
object Constants {
    const val BROADCAST_MESSAGE = "20"
    const val BASE_URL_CHANGED = "10"
    const val USERS_TOPIC = "users"
}