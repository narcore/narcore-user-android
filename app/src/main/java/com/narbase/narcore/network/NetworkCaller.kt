package com.narbase.narcore.network


import android.util.Base64
import com.narbase.narcore.persistence.PersistenceManager
import okhttp3.Credentials
import retrofit2.Call
import java.io.IOException

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 3/19/17.
 */
class NetworkCaller(
        private val api: MainApi,
        private val persistenceManager: PersistenceManager
) {

    suspend fun getToken(email: String, password: String) {
        val basicAuthHeader = Credentials.basic(email, password)
        val call = api.token(basicAuthHeader).executeCall()
    }

    suspend fun login(fcmToken: String) {
//        val call = api.login("Bearer ${persistenceManager.accessToken}", fcmToken).executeCall()
    }

    suspend fun checkVersionCode(versionCode: Int) = api.checkVersionCode(versionCode).executeCall()

    @Suppress("RedundantSuspendModifier")
    private suspend fun <T : BasicResponse> Call<T>?.executeCall(): T {
        if (this == null) throw ConnectionErrorException()
        try {
            val response = execute()
            if (response?.isSuccessful == true) {
                return response.body() ?: throw UnknownErrorException()
            } else if (response?.code() == 401) {
                throw UnauthorizedException()
            } else if (response?.code() == 400) {
                throw InvalidRequestException()
            } else if (response?.code() == 500) {
                throw UnknownErrorException()
            } else if (response?.code() == 405) {
                throw UnknownErrorException()
            } else {
                throw UnknownErrorException()
            }
        } catch (e: IOException) {
            throw ConnectionErrorException()
        }

    }

    private fun authBasicHeader(email: String, password: String) = "Basic ${Base64.encodeToString(("$email:$password").toByteArray(), Base64.NO_WRAP)}"

}
