package com.narbase.narcore.network

import com.google.gson.annotations.SerializedName
import com.narbase.narcore.network.CommonCodes.BASIC_SUCCESS

/**
* NARBASE TECHNOLOGIES CONFIDENTIAL
* ___________________________
* [2017] - [2018] Narbase Technologies
* All Rights Reserved.
* Created by islam
* On: 3/22/17.
*/

open class BasicResponse(
        @SerializedName("status")
        var status: String = "-1",

        @SerializedName("msg")
        var message: String? = null
)

class DataResponse<out T>(
        @SerializedName("data")
        val dto: T? = null,

        status: String = BASIC_SUCCESS,

        message: String? = null
) : BasicResponse(status, message)
