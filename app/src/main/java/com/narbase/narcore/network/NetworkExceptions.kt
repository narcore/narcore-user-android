package com.narbase.narcore.network

class ConnectionErrorException(msg: String = "") : Exception(msg)
class UnknownErrorException(msg: String = "") : Exception(msg)
class UnauthorizedException(msg: String = "") : Exception(msg)
class InvalidRequestException(msg: String = "") : Exception(msg)