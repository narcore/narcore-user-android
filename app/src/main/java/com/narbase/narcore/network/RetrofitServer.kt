package com.narbase.narcore.network

import com.narbase.narcore.network.oauth.AuthInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 3/19/17.
 */

object RetrofitServer {

//    val client: MainApi by lazy {
//        val verboseLogger = HttpLoggingInterceptor()
//        verboseLogger.level = HttpLoggingInterceptor.Level.BODY
//
//        val client = OkHttpClient.Builder()
//                .addInterceptor(verboseLogger)
//                .addInterceptor(AuthInterceptor())
//                .build()
//        val retrofit = Retrofit.Builder()
//                .baseUrl("")
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build()
//        retrofit.create(MainApi::class.java)
//    }
}
