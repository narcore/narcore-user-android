package com.narbase.narcore.network

import retrofit2.Call
import retrofit2.http.*

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by Islam
 * On: 3/19/17.
 */

interface MainApi {

    @POST("oauth/token")
    fun token(@Header("Authorization") authorization: String): Call<BasicResponse>

    @POST("api/driver/v1/login")
    fun login(@Header("Authorization") authorization: String,
              @Field("fcm_token") fcmToken: String
    ): Call<BasicResponse>


    @GET("api/public/v1/version")
    fun checkVersionCode(@Query("version_code") versionCode: Int): Call<BasicResponse>

}

