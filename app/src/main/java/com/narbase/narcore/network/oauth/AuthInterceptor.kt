package com.narbase.narcore.network.oauth

import com.narbase.narcore.persistence.PersistenceManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ___________________________
 * [2017] - [2018] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 4/30/17.
 */

class AuthInterceptor(
        private val persistenceManager: PersistenceManager) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        val request = chain.request()

        return if (persistenceManager.isLoggedIn && request.url().encodedPath() != "/oauth/token"){
            val builder = request.newBuilder()
            val authenticatedRequest = addAccessToken(builder)
            chain.proceed(authenticatedRequest)
        } else {
            chain.proceed(request)
        }
    }

    private fun addAccessToken(builder: Request.Builder): Request {
        val token = persistenceManager.accessToken
        builder.header("Authorization", token)
        return builder.build()
    }

}
