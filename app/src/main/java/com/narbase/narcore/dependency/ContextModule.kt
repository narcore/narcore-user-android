package com.narbase.narcore.dependency

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ContextModule(private var context: Context) {

    @Provides
    @Singleton
    fun context(): Context {
        return context
    }

}