package com.narbase.narcore.dependency

import com.narbase.narcore.network.MainApi
import com.narbase.narcore.network.NetworkCaller
import com.narbase.narcore.network.oauth.AuthInterceptor
import com.narbase.narcore.persistence.PersistenceManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ______________________________
 * [2013] - [2017] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 2018/04/15.
 */

@Module(includes = [PersistenceManagerModule::class])
class NetworkCallerModule {

    @Provides
    @Singleton
    fun networkCaller(mainApi: MainApi, persistenceManager: PersistenceManager): NetworkCaller {
        return NetworkCaller(mainApi, persistenceManager)
    }

    @Provides
    @Singleton
    fun mainApi(retrofit: Retrofit): MainApi {
        return retrofit.create(MainApi::class.java)
    }

    @Provides
    @Singleton
    fun retrofit(okHttpClient: OkHttpClient,
                 gsonConverterFactory: GsonConverterFactory,
                 persistenceManager: PersistenceManager): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(persistenceManager.baseUrl)
                .addConverterFactory(gsonConverterFactory)
                .build()
    }

    @Provides
    @Singleton
    fun gsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun okHttpClient(loggingInterceptor: HttpLoggingInterceptor,
                     authInterceptor: AuthInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(authInterceptor)
                .build()

    }

    @Provides
    @Singleton
    fun loggingInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    @Provides
    @Singleton
    fun authInterceptor(persistenceManager: PersistenceManager) = AuthInterceptor(persistenceManager)

}