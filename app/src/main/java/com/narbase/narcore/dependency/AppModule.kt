package com.narbase.narcore.dependency

import com.narbase.narcore.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {
    @Provides
    @Singleton
    fun app(): App  {
        return app
    }
}