package com.narbase.narcore.dependency

import com.narbase.narcore.App
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(app: App)
}