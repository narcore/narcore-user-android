package com.narbase.narcore.dependency

import android.content.Context
import com.narbase.narcore.App
import com.narbase.narcore.persistence.PersistenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * NARBASE TECHNOLOGIES CONFIDENTIAL
 * ______________________________
 * [2013] - [2017] Narbase Technologies
 * All Rights Reserved.
 * Created by islam
 * On: 2018/04/15.
 */

@Module(includes = [AppModule::class])
class PersistenceManagerModule {
    @Provides
    @Singleton
    fun providePersistenceManager(app: App): PersistenceManager {
        return PersistenceManager(app.applicationContext)
    }
}