package com.narbase.narcore.dependency

import com.narbase.narcore.network.NetworkCaller

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkCallerModule::class, PersistenceManagerModule::class])
interface NetworkCallerComponent {
    fun getNetworkCaller() : NetworkCaller
}