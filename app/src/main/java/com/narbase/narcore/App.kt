package com.narbase.narcore

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.narbase.narcore.dependency.AppComponent
import com.narbase.narcore.dependency.AppModule
import com.narbase.narcore.dependency.DaggerAppComponent


/**
* NARBASE TECHNOLOGIES CONFIDENTIAL
* ___________________________
* [2017] - [2018] Narbase Technologies
* All Rights Reserved.
* Created by islam
* On: 3/29/17.
*/

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        context = this
        component.inject(this)

//        Global.persistence = PersistenceManagerImplementation()
//        Global.serverCaller = ServerCaller()
//        FirebaseMessaging.getInstance().subscribeToTopic(USERS_TOPIC)
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }

    private val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .build()
    }

    companion object {
        lateinit var context: App
            private set
    }
}
